var purgecss = require('@fullhuman/postcss-purgecss');
var cssnano = require('cssnano');
var autoprefixer = require('autoprefixer');

const IS_DEV = process.env.NODE_ENV === 'development';

if (IS_DEV) {
  module.exports = {
    plugins: [
      autoprefixer
    ]
  }
} else {
  module.exports = {
    plugins: [
      cssnano({
        preset: 'default',
      }),
      purgecss({
        content: ['./src/**/*.pug'],
      }),
      autoprefixer
    ]
  }
}

// module.exports = {
//   theme: {},
//   variants: {},
//   plugins: [
//     require('tailwindcss')
//   ]
// }